#include <iostream>
#include <sstream>
#include "SkipBoGame.h"
#include <stdexcept>
#include "Player.h"
#include <string>
#include <fstream>
#include <istream>
#include <ctime>

using std::istream; using std::vector;
using std::exception; using std::cout;
using std::endl; using std::cerr;
using std::string; using std::invalid_argument;

/* for live game play - must not change format!

drawPile  build_a  build_b  build_c  build_d
playerName  stock_0  
discards: discard_1 discard_2 discard_3 discard_4
hand: card_5 card_6 card_7 card_8 card_9
 */
void SkipBoGame::display() const {
  std::cout << "Draw: ";
  draw.display();
  std::cout << "  Build Piles: ";
  for (int j = 0; j < 4; j++) {
    build[j].display();
    std::cout << " ";
  }
  std::cout << std::endl;
  peep[curp].display();
}

/* for saving state - must not change format!

shuffle numplayers currplayer
PlayerCurp [display]
PlayerCurp+1 [display]
[etc for all players]
Draw [display]
Build_a [display]
Build_b [display]
Build_c [display]
Build_d [display]
*/
std::string SkipBoGame::toString() const {
  std::stringstream result;
  int idx;
  result << draw.getRand() << " " << nump << " " << curp << "\n";
  for (int i = 0; i < nump; ++i) {
    idx = (curp+i) % nump;
    result << peep[idx].toString();
  }
  result << "Draw " << draw.toString(); 
  for (int j = 0; j < 4; j++) {
    result << "Build_" << char('a'+j) << " ";
    result << build[j].toString();  
  }
  return result.str();
}

// readin, perform the inverse of toString
void SkipBoGame::readIn(istream& is) {
  
  bool was_shuffled;
  std::string a_string;
  is >> a_string;  // extract whether or not is random
  was_shuffled = (a_string == "true");
  is >> nump;  // extract number of players
  is >> curp; // extract current player

  for(int i = 0; i < nump; i++)
    peep[(curp + i)%nump] = Player(is);  // need to supplement the readin method in player!!!
  
  is >> a_string;  //ignore name of draw pile
  draw = DrawPile(shuffled, is);

  if (shuffled && !was_shuffled)
    draw.shuffle();

  for(int j = 0 ; j < 4; j++)
    build[j].readIn(is);
}



//new game
SkipBoGame::SkipBoGame(bool shuffled, short num_players, short stock_pile_size, istream& deck_stream) :
  shuffled(shuffled), draw(DrawPile(deck_stream, shuffled)), nump(num_players) {

  //build now is initialized automatically
  for(short i=0; i<nump; i++) {
    std::stringstream ss;
    ss << "Player";
    ss << i;
    peep[i] = Player(ss.str());  //initialize Player objects
  }

  for(int i = 0; i < stock_pile_size; i++)
    for(int j = 0; j < nump; j++)
      peep[j].add_to_stock(draw);
 
  if (shuffled) {
    std::srand(unsigned(std::time(0)));
    curp = rand()%nump;  //randomly choose first player
  } else
    curp = 0;

}


//resume game
SkipBoGame::SkipBoGame(bool new_shuffled, istream& game_stream) : shuffled(new_shuffled) {
  readIn(game_stream);

  
  //for(int i = 0; i<nump; i++)
  //peep[(curp+i)%nump].draw_stock();
  
  //if (!shuffled && new_shuffled)  //was not shuffled, but now shuffled
      //  draw.shuffle();
  shuffled = new_shuffled;  //overwrite previous shuffle state
}




//read in all connected characters (important since chars are supposed to be connected)
//and return a bool indicating whether or not there were more than one char next to each other
//(which would make the input invalid);  the user's input is stored in input if valid

//if char is a number, input is set to its numerical representation (i.e., char-'0')
bool validate_input(string& block, char& input) {
  
  if (block.length() > 1)  //string is too long
    return false;
  
  std::stringstream ss(block);
  string tester;
  
  ss >> tester;  //extract first and (hopefully only) char in string along
  //with anything right next to it

  input = tester[0];
  
  input -= '0'*('0' <= input && input <= '9');  //implicit conditional to convert char to number
  
  return true;
}



//we're supposed to always try to read in the next two chars after an 'm', expecting them
//to be associated with it
bool SkipBoGame::prompt(char& move, char& source, char& dest) {
  
  cout << "Draw: ";  //print that first line of output
  draw.display();
  cout << "  Build Piles: ";
  for(unsigned char i = 0; i<4; i++) {
    build[i].display();
    cout << " ";
  }
  cout << endl;
  peep[curp].display();
  cout << "(m)ove [start] [end] or (d)raw ? ";
  
  string a_string;
  std::cin >> a_string;
  bool to_ret = validate_input(a_string, move);
  
  if (!to_ret)  //user entered multiple characters together
    return false;
  
  //only need to read in three chars if it's a move step
  if (move == 'd')
    return true;
  
  if (move == 'm') {
    std::cin >> a_string;
    to_ret = validate_input(a_string, source) && to_ret;  //swapped so that
    //short-circuiting doesn't prevent tokens from being read
    std::cin >> a_string;
    to_ret = validate_input(a_string, dest) && to_ret;
    
    return to_ret && (0 <= source && source <= 9) &&  //validate input
      ((0 <= dest && dest <= 9) || ('a' <= dest && dest <= 'd'));
  }

  throw invalid_argument("illegal command, try again");
  //neither 'd' nor 'm' if haven't returned yet, so invalid

}




void SkipBoGame::build_check(const char which) {
  if (build[which-'a'].size() == 12) {
    draw.set_aside(build[which-'a']);
  
    cout << "build pile " << which << " full, set aside" << endl;
    
    build[which-'a'] = BuildPile();  //replace full build pile
  }
}



void SkipBoGame::make_draw() {
  if (peep[curp].hand.size())  //cannot draw until hand is empty
    //only need to test for one more input to check if too many
    throw invalid_argument("illegal command, try again");
  else
    peep[curp].hand.refill(draw);

}


//returns true if player's turn is going to continue;  false otherwise
bool SkipBoGame::make_move(char source, char dest) {
 
    switch(source) {
	 
    case 0: {  //move from stock...
      switch (dest) {
      case 'a':
      case 'b':
      case 'c':
      case 'd': {  //...to build
	peep[curp].play_from_stock(build[dest-'a']);
	build_check(dest);  //set aside the build pile if need be
	
	if (peep[curp].has_won())  //if the stock pile is now empty, declare a winner
	  throw peep[curp].get_name();  //throw so as to skip all the prompts
	
	return true;  //turn will continue
      }
      default: { throw invalid_argument("illegal command, try again"); }
      }
      break;
    }
      
      //stock and discard must go to build pile      
    case 1:
    case 2:  //move from discards...
    case 3:
    case 4: {  //...to build
      if ('a' > dest || dest > 'd')
	throw invalid_argument("illegal command, try again");
      peep[curp].play_from_discard(source-1, build[dest-'a']);
      build_check(dest);
      
      return true;  //turn will continue
    }
      
      
    case 5:  //move from hand...
    case 6:
    case 7:
    case 8:
    case 9: {
      
      if (1 <= dest && dest <= 4) {  //...to discard pile
	peep[curp].play_from_hand(source-5, peep[curp].discard[dest-1]);
	
	return false;  //turn will END, as player discarded something
	
      } else if ('a' <= dest && dest <= 'd') {  //...to build pile
	peep[curp].play_from_hand(source-5, build[dest-'a']);
	build_check(dest);
      }
      else //invalid input
	throw invalid_argument("illegal command, try again");
      
      break;
    }
      //can't move anything from build piles to anywhere else,
      //so if a build pile is the source, it's invalid
      
    default: {
      throw invalid_argument("illegal command, try again"); }
    }

  return true;  //need to continue loop if invalid input
}






short SkipBoGame::play_turn() {
  peep[curp].hand.refill(draw);  //draw cards at the start of the turn
	
  char move;
  char source;
  char dest;	

	  while (true) {
 
	    try {
	      if (prompt(move, source, dest)) {
	      //display the game and extract all of the moves in accordance with the Piazza posts
	      
	      //!make_move = player's turn is over
	      if (move == 'm' && !make_move(source, dest)) {  //try to make a move
		cout << endl;  //need extra line
		break;  //end turn if discarded, meaning that make_move returned false
	      }
	      else if (move == 'd')  //try to draw cards
		make_draw();
	      else if (move != 'm')
		throw invalid_argument("illegal command, try again");
	      }
	      else
		cout << "illegal command, try again" << endl;
	    }
 
	    catch(string name) {  //there was a winner
	      return (short)(name[6]-'0');  //return winner number to main
	    }
	    
	    catch(exception e) {
	      cout << "illegal command, try again" << endl << endl;
	      //need extra line for the space
	      //between the lines since the "cout << endl;" line below won't run on this route
	      
	      continue;  //go back to the top of loop, taking in more user input
	    }
	    cout << endl;
	    
	  }
	curp++;  //increment player;  don't want to do this at beginning
	//so that can still call next_turn as first turn
	curp %= nump;  //wrap back to the first player
	
	return get_winner();  //continue loop in main() if there is no winner
}







short SkipBoGame::next_turn() {

  char input;
  string line;
  while(true) {
    
    cout << " >> " << peep[curp].get_name() << " turn next"
	 << endl << "(p)lay, (s)ave, or (q)uit ? ";
    std::cin >> line;
    try {
      if (validate_input(line, input) && (input == 'p' || input == 's' || input == 'q'))
	break;  //end loop
    else
      cout << "illegal command, try again" << endl;  //input is invalid
    } catch (exception) {
      cout << "illegal command, try again" << endl;
    }
  } 

      switch(input) {
      case 'p': {  //play
	return play_turn();  //continue loop in main if no winner
      }
      case 's': {  //save
	cout << "save filename: ";
	std::string file_name;
	std::cin >> file_name;

	std::ofstream file(file_name);
	if (!file.is_open())
	  throw std::invalid_argument("illegal command, try again");
	//Piazza clarification that we don't need to worry about write-protected file

	draw.rebuild();  //put the dead build piles back into the deck at the end of the game

	file << toString();
	
	file.close();  //recommended that we close the file
	return -2;
	
      }
      case 'q': {
	cout << "thanks for playing" << endl;
	
	return -2;
      }

      }

      return -1;  //an invalid input alone can not create a winner
}


//find winner
short SkipBoGame::get_winner() const {
  for (unsigned char i = 0; i < nump; i++)
    if (peep[i].has_won())
      return i;

  return -1;  //no winner
}

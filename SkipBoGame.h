#ifndef _SKIP_BO_GAME_H
#define _SKIP_BO_GAME_H

#include "Card.h"
#include "Pile.h"
#include "DrawPile.h"
#include "FaceUpPile.h"
#include "Player.h"
#include <istream>
#include <vector>

class SkipBoGame {
  
  bool shuffled;
  Player peep[6];
  BuildPile build[4];
  DrawPile draw;
  short curp;  //short so that still prints to file correctly without modifying toString()
  short nump;
  
  void readIn(std::istream& is);
  
  bool make_move(char source, char dest);  //returns true if player's turn is over;
  //false otherwise

  void make_draw();

  short play_turn();  //player plays a turn;  returns index of winner (or -1 if none)

  bool prompt(char& move, char& source, char& dest);

  void build_check(const char which);
  
 public:
  void display() const;
  
  std::string toString() const;
  
  //new game
  SkipBoGame(bool shuffled, short num_players, short stock_pile_size, std::istream& deck_stream);
  
  //restore old game
  SkipBoGame(bool new_shuffled, std::istream& game_stream);
  
  short next_turn();  //returns winner num if exists, -2 if quit or saved, -1 otherwise
  
  short get_winner() const;  //returns -1 if no winner yet;  index of winner otherwise
  
};

#endif

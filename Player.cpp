#include <iostream>
#include <string>
#include <sstream>
#include "Player.h"
#include <iostream>
#include <stdexcept>

using std::cin;
using std::cout;

// for live game play - must not change!
void Player::display() const {
  std::cout << "Current ";
  std::cout << name << ":  Stock{0}: " ;
  stock.display();
  std::cout << std::endl;
  std::cout << "   Discards{1-4}: ";
  for (int i = 0; i < 4; ++i) {
    discard[i].display();
    std::cout << "  ";
  }
  std::cout << std::endl;
  std::cout << "   Hand{5-9}: ";
  hand.display();
  std::cout << std::endl;
}

/* saving state format - must not change!
PlayerName
Stock size
01 02 03  ...
...
Hand size
01 02 03 04 05
Discard1 size
01 02 03 04 ...
...
Discard2 size
...
Discard3 size
...
Discard4 size
...
*/
std::string Player::toString() const {
  std::stringstream result;
  result << name << "\n";
  result << "Stock " << stock.toString();
  result << "Hand " << hand.toString();
  for (int i = 0; i < 4; ++i) {
    result << "Discard" << i << " " << discard[i].toString();
  }
  return result.str();
}


void Player::readIn(std::istream& is) {
  is >> name;

  stock.readIn(is);
  hand.readIn(is);

  for(int i = 0; i < 4; i++)
    discard[i].readIn(is);
}


Player::Player(std::string name) : name(name) {
  //hand is filled at the start of each turn, not at the beginning of the game
  
  //for(unsigned int i = 0; i < stock_size; i++)  //fill stock pile
    //stock.addCard(draw_pile.draw());
}




//attempt to play a card on a deck
void Player::play_from_hand(unsigned char index, Pile& deck) {
    hand.play(index, deck);
}

void Player::play_from_stock(Pile& deck) {
    deck.addCard(stock.get_top());  //if this fails...
    stock.pull_off();  //...this never runs
}

void Player::play_from_discard(unsigned char index, Pile& deck) {
    deck.addCard(discard[index].get_top());
    discard[index].pull_off();  //only runs if prior line succeeded
}

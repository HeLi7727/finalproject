#include <iostream>
#include "Hand.h"
#include "DrawPile.h"
#include <stdexcept>

/*************************************
For SkipBo - Fall 2018 - EN.601.220
Instructor provided code
*************************************/

// for live game play - must not change!
void Hand::display() const {
  int i;
  for (i = 0; i < size(); i++) {
    pile[i].display();
    std::cout << "  ";
  }
  for ( ; i < 5; i++)
    std::cout << "--  ";
}

void Hand::refill(DrawPile& draw_pile) {
  //fill the hand to five
  for(int i=size(); i<5; i++)
    addCard(draw_pile.draw());

}

void Hand::readIn(std::istream& is) {
  std::string a_string;
  is >> a_string;

  short size;
  is >> size;

  int card_value;
  for(unsigned char i = 0; i < size; i++) {
    is >> card_value;
    addCard(Card(card_value));
  }
  }

void Hand::play(char index, Pile& deck) {
  if (index >= size())
    throw std::out_of_range("invalid command, try again4");

  deck.addCard(pile[index]);  //if this fails, the next line won't execute
  pile.erase(pile.begin() + index);  //destroy this card

}

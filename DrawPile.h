#ifndef _DRAW_PILE_H
#define _DRAW_PILE_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <istream>
#include "FaceUpPile.h"
#include <ctime>

class DrawPile : public Pile {
  
 private:
  std::vector<BuildPile> dead_builds;

  bool shuffled;

  //add to bottom, not top, as per Piazza
  void addCard(const Card& c) { pile.push_back(c); }  //intentionally hiding this method,
  //as should only be able to add cards to draw pile after they're reclaimed
  //from completed build piles (is there any other reason to add cards here...?)
  
 public:

  void rebuild();
  
  void set_aside(BuildPile& build) { dead_builds.push_back(build); }
  
  //void readIn(std::istream& is);
  
  void shuffle() { std::srand(unsigned(std::time(0)));
    std::random_shuffle(pile.begin(), pile.end()); }
  
  void display() const;
  
  Card draw();

  //for resume game
 DrawPile(const bool shuffled, std::istream& is) : shuffled(shuffled) { readIn(is); }
  
  //for new game
  DrawPile(std::istream& is, const bool shuffled);

  DrawPile() { }
  
  
  std::string getRand() const;
  
};


#endif

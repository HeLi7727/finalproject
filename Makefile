CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

skipbo: main.o Card.o Pile.o DrawPile.o FaceUpPile.o Hand.o Player.o SkipBoGame.o
	g++ -o skipbo main.o Card.o Pile.o DrawPile.o FaceUpPile.o Hand.o Player.o SkipBoGame.o

Card.o: Card.h Card.cpp
	g++ -c Card.cpp $(CFLAGS)

Pile.o: Pile.h Pile.cpp Card.h
	g++ $(CFLAGS) -c Pile.cpp 

DrawPile.o: DrawPile.h DrawPile.cpp Pile.h Card.h
	g++ $(CFLAGS) -c DrawPile.cpp

Hand.o: Card.h Pile.h DrawPile.h Hand.h Hand.cpp
	g++ $(CFLAGS) -c Hand.cpp

FaceUpPile.o: Card.h Pile.h DrawPile.h FaceUpPile.h FaceUpPile.cpp
	g++ $(CFLAGS) -c FaceUpPile.cpp

Player.o: Card.h Pile.h DrawPile.h Hand.h FaceUpPile.h Player.h Player.cpp
	g++ $(CFLAGS) -c Player.cpp

SkipBoGame.o: Card.h Pile.h DrawPile.h Hand.h Hand.cpp FaceUpPile.h Player.h SkipBoGame.h SkipBoGame.cpp
	g++ $(CFLAGS) -c SkipBoGame.cpp

main.o: main.cpp SkipBoGame.h Card.h Pile.h DrawPile.h Hand.h Hand.cpp FaceUpPile.h Player.h SkipBoGame.h
	g++ $(CFLAGS) -c main.cpp

.PHONY: clean
clean:
	rm -f *.o skipbo?  *~ skipbo

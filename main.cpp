#include <iostream>
#include <fstream>
#include "SkipBoGame.h"
#include <stdexcept>
#include "Player.h"
#include <iostream>

using std::cerr; using std::string;
using std::cout; using std::endl;

//really did not feel that default constructor for SkipBoGame was "appropriate,"
//so I wrote this routine to reduce reused code
void play_game(SkipBoGame& game) {
  short winner;
  do
    winner = game.next_turn();
  while (winner == -1);

  if (winner != -2)  //-2 = done playing (saved or quit);  this would cause the loop to break
    cout << endl << "GAME OVER - Player" << winner << " wins!" << endl;

}


int main(int argc, char **argv) {

  // wrong number of argument check
  // fits for either new or reload game
  if (argc != 5 && argc != 3) {
    cout << "invalid program usage: invalid number of arguments" << endl;
    return 1;
  }

  // second argument check
  // error check fits either new or reload game
  bool shuffled;
  if(string(argv[1]) != "false" && string(argv[1]) != "true") {
    cout << "invalid program usage: invalid first argument" << endl;
    return 1;
  } else
    shuffled = (string(argv[1]) == "true");
  //we now know that the word is either true or false, so this is valid

  // reload game
  if(argc == 3) {
    std::ifstream fin;
    fin.open(argv[2], std::ifstream::in);
    if(!fin.is_open()) {
      cout << "invalid program usage: can't open input game file" << endl;
      return 1;
    }
    // here, good to start the reload
    
    SkipBoGame game(shuffled, fin);
    //fin.close();
    char winner = game.get_winner();
    if (winner >= 0)  //game already has a winner
      cout << endl << "GAME OVER - Player" << winner << " wins!" << endl;
    else
      play_game(game);
    return 0;

  }

  // New game;  argc is guaranteed == 5
  int num_of_player = 0;
  int stock_size = 0;
  
  num_of_player = atoi(argv[2]);
  if (num_of_player < 2 || num_of_player > 6)  {
    cout << "invalid program usage: num players must be 2-6" << endl;
    return 1;
  } 
  std::cout << "num players is " << num_of_player << endl;
  stock_size = atoi(argv[3]);
  if ((stock_size < 1 || stock_size > 30) || (num_of_player == 6 && stock_size > 20)) {
    cout << "invalid program usage: bad stock size" << endl;
    return 1;
  }
  std::cout << "stock size is " << stock_size << endl << endl;
  std::ifstream fin2;
  fin2.open(argv[4], std::ifstream::in);
  if(!fin2.is_open()) {
    cout << "invalid program usage: can't open deck file" << endl;
    return 1;
  }
  
  // here, good to start a new game
  
  SkipBoGame game(shuffled, num_of_player, stock_size, fin2);
  //fin2.close();
  
  play_game(game);
  
  return 0;
}

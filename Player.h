#ifndef _PLAYER_H
#define _PLAYER_H

#include "DrawPile.h"
#include "FaceUpPile.h"
#include "Hand.h"
#include <vector>
#include <istream>
#include <string>

class Player {
  std::string name;
  Hand hand;
  PlayPile stock;
  PlayPile discard[4];
  friend class SkipBoGame;
  
 public:
  std::string get_name() const { return name; }
  
  void display() const;

  std::string toString() const;

  void readIn(std::istream& is);

  Player() { }

  //new player
  Player(std::string name);
  
  //load player from restored game (consider moving stream extraction here, in which
  //case do not pass it these parameters)
  Player(std::istream& is) { readIn(is); }

  //use a short so as not to get a warning that index is of type char
  //void discard_top(short which) { discard[which].pull_off(); }

  void draw(DrawPile& from) { hand.refill(from); }

  void add_to_stock(DrawPile& draw) { stock.addCard(draw.draw()); }

  //Card read_stock() const { return stock.get_top(); }

  //void destroy_stock_top() { stock.pull_off(); }

  void play_from_hand(unsigned char index, Pile& deck);

  void play_from_stock(Pile& deck);

  void play_from_discard(unsigned char index, Pile& deck);

  bool has_won() const { return !stock.size(); }
  
};

#endif

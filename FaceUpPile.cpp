#include <iostream>
#include "Card.h"
#include "Pile.h"
#include "DrawPile.h"
#include "FaceUpPile.h"
#include <stdexcept>
#include <istream>
#include <string>

// for live game play - must not change!
void FaceUpPile::display() const {
  if (size()) {
    pile[pile.size()-1].display();
  }
  else std::cout << "--";
  std::cout << "/" << size();
}

//void FaceUpPile::readIn(std::istream& is) {
//Pile::readIn(is);
//}

void FaceUpPile::readIn(std::istream& is) {
  //short ignore;
  //is >> ignore;

  std::string a_string;
  is >> a_string;
  //std::cout << a_string << "." << std::endl;
  
  //Pile::readIn(is);

  short size;
  is >> size;
  int card_value;
  for(short i = 0; i < size; i++) {
    is >> card_value;
    addCard(Card(card_value));
  }
  }




Card FaceUpPile::get_top() const {
  if (size())
    return pile[size()-1];

  throw std::invalid_argument("illegal command, try again");
}


void BuildPile::addCard(const Card& card) {
  //ensure that card's value is either SkipBo or one greater than top card's value
  if (size() < 12 && (card.getValue() == 0 || (!size() && card.getValue() == 1) ||
    card.getValue() == 1+size()))  //current value looking for increases with size
    		pile.push_back(card);
  else
	  throw std::invalid_argument("illegal command, try again");  //cannot add this card
}

Card FaceUpPile::pull_off() {
	if (size()) {
    		Card pulled(pile[size()-1]);
    		pile.pop_back();
    		return pulled;
  	}
  	throw std::out_of_range("illegal command, try again5");  //tried to draw from an empty deck
}

void PlayPile::play(Pile& to_deck) {
  if (!size())
    throw std::out_of_range("illegal command, try again4");
  else
    try {
      to_deck.addCard(pull_off());  //attempt to add the top-most card
    } catch(std::exception) {
      throw std::invalid_argument("illegal command, try again");
    }
}

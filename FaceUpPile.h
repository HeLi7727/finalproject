#ifndef _FACE_UP_PILE_H
#define _FACE_UP_PILE_H

#include "Pile.h"
#include "DrawPile.h"
#include "Card.h"
#include <istream>

class FaceUpPile : public Pile {
	protected:
 		FaceUpPile() { };  //this is an abstract class
  
 	public:
  		virtual void display() const;

	        void readIn(std::istream& is);

  		Card get_top() const;

		Card pull_off();  //pull off the top-most card

};

class BuildPile : public FaceUpPile {
  
	public:

  
  BuildPile() { }

  BuildPile(std::istream& is) { readIn(is); }

  		void addCard(const Card& card) override;

		//void readIn(std::istream& is) override;

 //BuildPile only uses pull_off() when being cannibalized by DrawPile
  
};

class PlayPile : public FaceUpPile {
  
	public:
  
  		PlayPile() { }

  		PlayPile(std::istream& is) { readIn(is); }

		void play(Pile& to_deck);  //attempt to play a card to a pile

		//void readIn(std::istream& is) override;
};

#endif

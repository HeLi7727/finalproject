#include <iostream>
#include "DrawPile.h"
#include <vector>
#include <stdexcept>
#include <istream>
#include "FaceUpPile.h"

using std::vector;

// for live game play - must not change!
void DrawPile::display() const {
  std::cout << "[XX]/" << size();
}

Card DrawPile::draw() {
  if (size()) {  //still need this conditional here to check for the first time in a game
    Card drawn(pile[size()-1]);
    pile.pop_back();  //Piazza says to treat this as a stack
    
    if (!size())  //draw pile is empty;  rebuild
      rebuild();	    
    return drawn;
  }
  throw std::out_of_range("invalid command, try again6");  //tried to draw from an empty deck
}

//new game
DrawPile::DrawPile(std::istream& is, const bool shuffled) : shuffled(shuffled) {
  
  int card_value;
  while(is >> card_value)
    addCard(Card(card_value));

  if (shuffled)
    shuffle();

}



//after draw pile runs out of cards, reclaim them from the build piles
void DrawPile::rebuild() {

  //as much as I would like to start at size() (more efficient),
  //I assume that the unshuffled program probably adds them in order

  bool should_shuffle = shuffled && dead_builds.size() > 0;
  
  while (dead_builds.size()) {
 
    for(unsigned char i = 0; i < 12; i++)
      pile.insert(pile.begin(), dead_builds[dead_builds.size()-1].pull_off());
    //pop a card from the back of a pile to the beginning of this one
    
    dead_builds.pop_back();  //last pile is now empty
  }

  if (should_shuffle)
    shuffle();
  
}

std::string DrawPile::getRand() const {
  std::string rand = "false";
  if(shuffled) {
    rand = "true";
  }
  return rand; 
  
}


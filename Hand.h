#ifndef _HAND_H
#define _HAND_H

#include "DrawPile.h"
#include <istream>

class Hand : public Pile {

	public:
  		void display() const;

  		Hand() {}

  		Hand(std::istream& is) { readIn(is); }

		void readIn(std::istream& is) override;
		
  		void refill(DrawPile& draw_pile);  //call this at the start of each turn

  		void play(char index, Pile& deck);

		Card get(unsigned char index) { return pile[index]; }

};

#endif
